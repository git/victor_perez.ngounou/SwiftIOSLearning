//
//  ContentView.swift
//  About Me
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            HomeView().tabItem{
                    Label("About Me",systemImage: "person")
                }
                .foregroundColor(.accentColor)
            StoryView().tabItem{
                Label("Story",systemImage: "book")
            }.foregroundColor(.accentColor)
            FavoritesViews().tabItem{
                Label("Favorites",systemImage: "star")
            }
            FunFactsView()
                .tabItem{
                    Label("Fun Facts",systemImage: "hand.thumbsup")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
