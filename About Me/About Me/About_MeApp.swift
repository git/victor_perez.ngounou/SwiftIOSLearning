//
//  About_MeApp.swift
//  About Me
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

@main
struct About_MeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
