//
//  FavoritesViews.swift
//  About Me
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

struct FavoritesViews: View {
    var body: some View {
        VStack{
            Text("Favorites")
                .font(.largeTitle)
                .fontWeight(.bold)
                .padding(.bottom,40)
            Text("Hobbies")
                .font(.title2)
            HStack{
                ForEach(information.hobbies,id: \.self){
                    hobbie in
                    Image(systemName: hobbie)
                        .resizable()
                        .frame(maxWidth: 80,maxHeight: 60)
                }
            }
            .padding()
            Text("Foods")
                .font(.title2)
            HStack(spacing: 60){
                ForEach(information.foods, id: \.self){
                    food in
                    Text(food).font(.system(size: 48))
                }
            }
        }
        .padding()
        
    }
}

struct FavoritesViews_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesViews()
    }
}
