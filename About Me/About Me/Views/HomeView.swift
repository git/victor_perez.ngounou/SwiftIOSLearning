//
//  HomeView.swift
//  About Me
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        VStack{
            Text("All About Me")
                .font(.title)
                .fontWeight(.bold)
                .padding()
            Image(information.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .cornerRadius(10)
                .padding(40)
            Text(information.name)
                .font(.title)
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
