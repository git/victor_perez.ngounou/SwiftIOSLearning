//
//  FunFactsView.swift
//  About Me
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

struct FunFactsView: View {
    @State private var funFac=""
    var body: some View {
        VStack{
            Text("Fun Facts")
                .font(.largeTitle)
                .fontWeight(.bold)
            
            Text(funFac)
                .padding()
                .font(.title)
                .frame(minWidth: 400)
            Button("Show random Fact"){
                funFac=information.funFacts.randomElement()!
            }
        }
        .padding()
    }
}

struct FunFactsView_Previews: PreviewProvider {
    static var previews: some View {
        FunFactsView()
    }
}
