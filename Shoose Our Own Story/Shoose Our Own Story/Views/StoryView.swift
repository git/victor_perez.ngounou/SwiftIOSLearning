//
//  StoryView.swift
//  Story Data Model
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

struct StoryView: View {
    var body: some View {
        NavigationStack{
            StoryPageView(story: story, pageIndex: 0)
        }
    }
}

struct StoryView_Previews: PreviewProvider {
    static var previews: some View {
        StoryView()
    }
}
