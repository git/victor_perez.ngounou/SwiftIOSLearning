//
//  StoryPageView.swift
//  Story Data Model
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

struct StoryPageView: View {
    let story:Story
    let pageIndex:Int
    var body: some View {
        VStack{
            ScrollView{
                Text(story[pageIndex].text)
            }
            
            ForEach(story[pageIndex].choices, id: \Choice.text){
                choise in
                NavigationLink(destination: StoryPageView(story: story, pageIndex: choise.destination)){
                    Text(choise.text)
                        .multilineTextAlignment(.leading)
                        .frame(maxWidth: .infinity,alignment: .leading)
                        .padding()
                        .background(Color.gray.opacity(0.25))
                        .cornerRadius(10)
                }
            }
        }
        .padding()
        .navigationTitle("Page \(pageIndex + 1)")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct StoryPageView_Previews: PreviewProvider {
    static var previews: some View {
        StoryPageView(story: story, pageIndex: 0)
    }
}
