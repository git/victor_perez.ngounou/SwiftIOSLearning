//
//  Story_Data_ModelApp.swift
//  Story Data Model
//
//  Created by etudiant on 09/05/2023.
//

import SwiftUI

@main
struct Story_Data_ModelApp: App {
    var body: some Scene {
        WindowGroup {
            StoryView()
        }
    }
}
